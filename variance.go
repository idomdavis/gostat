package gostat

import (
	"math"
)

// Online or running set of statistics that doesn't need to keep all the values
// to produce stats. Based on https://www.johndcook.com/blog/skewness_kurtosis/
//
// Note: Altering the values of an Online statistic will invalidate the figures
// it produces.
type Online struct {
	Sample bool

	tally int

	Count int

	Total float64
	Max   float64
	Min   float64
	Mean  float64

	M2 float64
	M3 float64
}

// Tally records an increment to a value without pushing it to the online set.
// When PushTally is called the current tally count will be pushed.
func (o *Online) Tally(n int) {
	o.tally += n
}

// PushTally pushes the current tally count to the online set. The tally count
// is reset to 0.
func (o *Online) PushTally() {
	o.Push(o.tally)
	o.tally = 0
}

// Push an integer value into the online set, updating the statistics to reflect
// the change.
func (o *Online) Push(x int) {
	o.PushFloat(float64(x))
}

// PushFloat pushes a float64 value into the online set, updating the statistics
// to reflect the change.
func (o *Online) PushFloat(x float64) {
	var delta, mean, t float64

	if o.Count < 0 {
		o.Count = 0
	}

	if o.Count == 0 {
		o.Min = math.MaxFloat64
	}

	if x < o.Min {
		o.Min = x
	}

	if x > o.Max {
		o.Max = x
	}

	o.Count++
	o.Total += x
	delta = x - o.Mean
	mean = delta / float64(o.Count)
	t = delta * mean * float64(o.Count-1)

	o.Mean += mean
	//nolint:gomnd
	o.M3 += t*mean*float64(o.Count-2) - 3*mean*o.M2
	o.M2 += t
}

// Variance of the online set. If the count of items pushed is 1 then
// Variance will return 0.
func (o *Online) Variance() float64 {
	var d float64

	if o.Sample {
		d = float64(o.Count - 1)
	} else {
		d = float64(o.Count)
	}

	if d == 0 {
		return 0
	}

	return o.M2 / d
}

// StandardDeviation of the online set.
func (o *Online) StandardDeviation() float64 {
	return math.Sqrt(o.Variance())
}

// Skewness of the online set. A positive skewness typically means that the
// right-hand tail will be longer than the left-hand tail, and vice-versa for a
// negative tail. Skewness can return NaN under some conditions.
//
// Note: Skewness is heavily dependent on sample size. For Count < 5000 the
// figure may not be accurate.
func (o *Online) Skewness() float64 {
	//nolint:ifshort
	d := math.Pow(o.M2, 1.5)

	if d == 0 {
		return math.NaN()
	}

	return math.Sqrt(float64(o.Count)) * o.M3 / d
}
