# gostat

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/gostat/main?style=plastic)](https://bitbucket.org/idomdavis/gostat/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/gostat?style=plastic)](https://bitbucket.org/idomdavis/gostat/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/gostat?style=plastic)](https://bitbucket.org/idomdavis/gostat/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/gostat)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

An implementation of an Online (running) set of statistics for Golang.
