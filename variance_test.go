package gostat_test

import (
	"fmt"
	"math"
	"testing"

	"bitbucket.org/idomdavis/gostat"
)

func ExampleOnline_Push() {
	o := gostat.Online{}
	o.Push(1)
	o.Push(2)
	o.Push(3)

	fmt.Println(o.Count, o.Max, o.Min, o.Mean, o.Total)

	// Output:
	// 3 3 1 2 6
}

func ExampleOnline_PushTally() {
	o := gostat.Online{}
	o.Tally(1)
	o.PushTally()
	o.Tally(1)
	o.Tally(1)
	o.PushTally()
	o.Tally(3)
	o.PushTally()
	o.Tally(1)

	fmt.Println(o.Count, o.Max, o.Min, o.Mean, o.Total)

	// Output:
	// 3 3 1 2 6
}

func ExampleOnline_Variance() {
	o := gostat.Online{Sample: true}

	o.Push(1)

	fmt.Println(o.Variance())

	o.Push(1)
	o.Push(4)

	fmt.Println(o.Variance())

	o.Sample = false

	fmt.Println(o.Variance())

	// Output:
	// 0
	// 3
	// 2
}

func ExampleOnline_StandardDeviation() {
	o := gostat.Online{}

	o.Push(1)
	o.Push(2)

	fmt.Println(o.StandardDeviation())

	o.Sample = true

	fmt.Println(o.StandardDeviation())

	// Output:
	// 0.5
	// 0.7071067811865476
}

func ExampleOnline_Skewness() {
	o := gostat.Online{}

	o.Push(1)
	o.Push(1)
	o.Push(2)

	fmt.Println(o.Skewness())

	o.Push(2)
	o.Push(2)

	fmt.Println(o.Skewness())

	// Output:
	// 0.7071067811865475
	// -0.4082482904638631
}

func TestOnline_PushFloat(t *testing.T) {
	t.Run("Negative counts will reset", func(t *testing.T) {
		o := gostat.Online{Count: -1}
		o.PushFloat(1)

		if o.Count != 1 {
			t.Errorf("Expected Count of 1, got %d", o.Count)
		}
	})
}

func TestOnline_Skewness(t *testing.T) {
	t.Run("0 divisor wont error", func(t *testing.T) {
		o := gostat.Online{}

		s := o.Skewness()

		if !math.IsNaN(s) {
			t.Errorf("Expected Skewness of NaN, got %f", s)
		}
	})
}
